package com.teknei.bid.controller.rest;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.RequestEncFilesDTO;
import com.teknei.bid.service.FacialService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

@RestController
@RequestMapping(value = "/facial")
public class FacialController {

    @Autowired
    @Qualifier(value = "facialCommand")
    private Command facialCommand;
    @Autowired
    private FacialService facialService;
    private static final Logger log = LoggerFactory.getLogger(FacialController.class);

    @ApiOperation(value = "Uploads a picture to the casefile related to the given customer on base64 format")
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> uploadPlain(@RequestBody RequestEncFilesDTO requestEncFilesDTO){
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".uploadPlain  Uploads a picture to the casefile related to the given customer on base64 format");
        CommandRequest request = new CommandRequest();
        request.setId(requestEncFilesDTO.getOperationId());
        request.setFileContent(Arrays.asList(Base64Utils.decodeFromString(requestEncFilesDTO.getB64Anverse())));
        request.setUsername(requestEncFilesDTO.getUsername());
        CommandResponse response = facialCommand.execute(request);
        if (response.getStatus().equals(Status.FACIAL_OK)) {
            return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
        }
        JSONObject jsonObject = new JSONObject();
        if (response.getDesc().equals(String.valueOf(Status.FACIAL_MBSS_ERROR.getValue()))) {
            Long idFound = response.getId();
            jsonObject.put("id", idFound);
            jsonObject.put("status", String.valueOf(response.getDesc()));
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            jsonObject.put("status", String.valueOf(response.getDesc()));
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Uploads a picture to the casefile related to the given customer", notes = "The attachment must be named 'file'", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the picture is saved successfully"),
            @ApiResponse(code = 422, message = "If the picture could not be saved to the casefile"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/upload/{id}", method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity<String> uploadPhoto(@RequestPart("file") MultipartFile file, @PathVariable Long id) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".uploadPhoto id:"+id);
        CommandRequest request = new CommandRequest();
        request.setId(id);
        request.setFiles(Arrays.asList(file));
        request.setUsername("NA");
        CommandResponse response = facialCommand.execute(request);
        if (response.getStatus().equals(Status.FACIAL_OK)) {
            return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
        }
        JSONObject jsonObject = new JSONObject();
        if (response.getDesc().equals(String.valueOf(Status.FACIAL_MBSS_ERROR.getValue()))) {
            Long idFound = response.getId();
            jsonObject.put("id", idFound);
            jsonObject.put("status", String.valueOf(response.getDesc()));
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            jsonObject.put("status", String.valueOf(response.getDesc()));
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Downloads the picture related to the face of the given customer", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the picture is found successfully"),
            @ApiResponse(code = 422, message = "If the picture could not be found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getImageFromReference(@RequestBody DocumentPictureRequestDTO dto) {
    	//log.info("lblancas:: [tkn-service-facial] :: "+this.getClass().getName()+".getImageFromReference ");
        byte[] image = null;
        try {
            image = facialService.findPicture(dto);
            if (image == null) {
                return new ResponseEntity<>(image, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(image, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding image for reference: {} with message: {}", dto, e.getMessage());
            return new ResponseEntity<>(image, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
